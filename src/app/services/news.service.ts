import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RespuestaTopHeadlines } from '../interfaces/interfaces';
@Injectable({
  providedIn: 'root'
})


export class NewsService {

  constructor(private http: HttpClient) {}
    getTopHeadlines(){
      return this.http.get<RespuestaTopHeadlines>(`http://newsapi.org/v2/everything?q=bitcoin&from=2020-02-03&sortBy=publishedAt&apiKey=50c9149eb5854cce9a6d11d25a459e8e`
      );
    }

   }

